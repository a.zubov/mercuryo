# Tests for Mercuryo

### Clone repo

`git clone https://gitlab.com/a.zubov/mercuryo.git`

### Run script with your query:

`sudo ./mercuryo/run.sh --query "your query"`
