# -*- coding: utf-8 -*-
import pytest
from selene import browser, config
from selene.browsers import BrowserName
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


# Webdriver Initialize part

# selenoid - для запуска на сервере Selenoid!
# важно - нужно поменять ниже айпишник и порт
@pytest.fixture(scope="module")
def chrome_selenoid():

    capabilities = {
        "browserName": "chrome",
        "version": "71.0",
        "enableVNC": False,
        "enableVideo": False
    }
    driver = webdriver.Remote(command_executor="http://localhost:4444/wd/hub", desired_capabilities=capabilities)
    browser.set_driver(driver)


@pytest.fixture(scope="module")
def firefox_selenoid():

    capabilities = {
        "browserName": "firefox",
        "version": "63.0",
        "enableVNC": False,
        "enableVideo": False
    }
    driver = webdriver.Remote(command_executor="http://localhost:4444/wd/hub", desired_capabilities=capabilities)
    browser.set_driver(driver)


# Parsing arguments part
def pytest_addoption(parser):
    parser.addoption("--query", action="store")

@pytest.fixture(scope='session')
def query(request):
    name_value = request.config.option.query
    if name_value is None:
        pytest.skip()
    return name_value





# local_chrome & local_firefox - использовал для запуска тестов на локальной машине для отладки
@pytest.fixture(scope="module")
def local_chrome():

    config.browser_name = BrowserName.CHROME
    options = Options()

    options.add_argument("--start-maximized")
    driver = webdriver.Chrome(options=options, executable_path=r"C:\projects\python\Drivers\chromedriver.exe")
    browser.set_driver(driver)

    # close webdriver
    yield browser
    browser.quit()


@pytest.fixture(scope="module")
def local_firefox():

    config.browser_name = BrowserName.FIREFOX
    driver = webdriver.Firefox(executable_path=r"C:\projects\python\Drivers\geckodriver64.exe")
    browser.set_driver(driver)

    # close webdriver
    yield browser
    browser.quit()
