# -*- coding: utf-8 -*-
import time

from selene import browser
from selene.support.jquery_style_selectors import s, ss


class SearchGoogle(object):

    def __init__(self):
        # open Google in Constructor
        browser.open_url("http://google.com")


    def get_search(self, query):
        s("[name='q']").set_value(query).submit()

        time.sleep(3)   # add for Firefox
        search_results = browser.driver().find_elements_by_xpath("//h3/ancestor::a")

        # print
        print('\n + '"Results for query: {} ".format(query))
        print("-----------------------------")
        for search_result in search_results:
            print(str(search_result.text) + '\n')



