#!/usr/bin/env bash

cd mercuryo

# Download docker image for Chrome 71.0
docker pull selenoid/chrome:71.0

# Download docker image for Firefox 63.0
docker pull selenoid/firefox:63.0

# run selenoid
docker run -d --rm --name selenoid -p 4444:4444 -v `pwd`/browsers.json:/etc/selenoid/browsers.json:ro \
      -v /var/run/docker.sock:/var/run/docker.sock aerokube/selenoid

# build tests
docker build -t mercuryo_test .

# run tests
docker run --rm --net host mercuryo_test pytest --tests-per-worker 2 -sv --query "$2"

# Well Done!
