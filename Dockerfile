FROM zubovav/my_python

MAINTAINER Alexey Zubov <zubov.alexey@gmail.com>

EXPOSE 4444

COPY . /app
WORKDIR /app

RUN pipenv install --deploy --system